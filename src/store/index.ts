import stationsStore from './StationsStore';

const store = {
  stationsStore,
};

export default store;
