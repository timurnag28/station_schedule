import {action, makeAutoObservable, runInAction} from 'mobx';
import {Animated, Easing} from 'react-native';
import {SfProTextRegular, StationData, WINDOW_HEIGHT} from '../share/consts/consts';
import {onGetStationArrivals} from '../api/StationArrivalsApi';
import {TrainData} from '../share/interfaces/interfaces';
import {showMessage} from 'react-native-flash-message';

class StationsStore {
  constructor() {
    makeAutoObservable(this);
  }
  stationList = StationData;
  isActiveStationModal = false;
  stationCode = 'CNLLY';
  trainsList: TrainData[] = [];
  sortedTrainsList: TrainData[] = [];
  isLoading = true;
  currentStation = 'Connolly';
  headerLettersNumber = 10;
  currentDirection = 'Northbound';
  lastUpdateMinutes = 0;
  setIntervalMinutes: any;

  @action
  async onGetStationArrivals() {
    await this.loadData('CNLLY');
  }

  @action
  onSelectStation = async (StationCode: string, StationDesc: string) => {
    this.isLoading = true;
    this.onShowStationsModal();
    this.onCountHeaderLetters(StationDesc.length);
    this.stationCode = StationCode;
    this.currentStation = StationDesc;
    await this.loadData(StationCode);
    this.scrollY.setValue(0);
  };

  @action
  onSetLastUpdate = () => {
    this.setIntervalMinutes = setInterval(() => {
      runInAction(() => {
        this.lastUpdateMinutes = this.lastUpdateMinutes + 1;
      });
    }, 60000);
  };

  @action
  onSetDirection = async () => {
    if (!this.isLoading) {
      if (this.currentDirection === 'Southbound') {
        this.currentDirection = 'Northbound';
        showMessage({
          titleStyle: {
            color: '#fff',
            fontSize: 17,
            fontFamily: SfProTextRegular,
            fontWeight: 'bold',
          },
          message: 'North direction has been selected.',
          type: 'info',
          backgroundColor: '#8CD76A',
          color: '#fff',
        });
      } else {
        this.currentDirection = 'Southbound';
        showMessage({
          titleStyle: {
            color: '#fff',
            fontSize: 17,
            fontFamily: SfProTextRegular,
            fontWeight: 'bold',
          },
          message: 'South direction has been selected.',
          type: 'success',
          backgroundColor: '#8CD76A',
          color: '#fff',
        });
      }

      await this.loadData(this.stationCode);
    }
  };

  @action
  loadData = async (StationCode: string) => {
    this.isLoading = true;
    this.onStopLastUpdate();
    this.onSetLastUpdate();
    await onGetStationArrivals(StationCode)
      .then(response => {
        runInAction(() => {
          if (response !== null) {
            this.trainsList = [];
            this.isLoading = false;
            this.trainsList = response;
            let result = this.trainsList?.filter(
              train => train.Direction === this.currentDirection,
            );
            this.sortedTrainsList = [];
            this.sortedTrainsList.push(...result);
          } else {
            this.trainsList = [];
            this.isLoading = false;
          }
        });
      })
      .catch(err => console.log(err));
  };

  @action
  onShowStationsModal = () => {
    this.isActiveStationModal = !this.isActiveStationModal;
  };

  @action
  onCountHeaderLetters(value: number) {
    this.headerLettersNumber = value;
  }

  @action
  onStopLastUpdate = () => {
    this.lastUpdateMinutes = 0;
    clearInterval(this.setIntervalMinutes);
  };

  @action
  determineViewColor = (Traintype: string) => {
    switch (Traintype) {
      case 'DART':
        return '#e9efe3';
      case 'COMMUTER':
        return '#e2e7f0';
      case 'INTERCITY':
        return '#eeede3';
      default:
        return '#e9efe3';
    }
  };

  @action
  determineTextColor = (text: string) => {
    switch (text) {
      case 'DART':
        return '#44b594';
      case 'COMMUTER':
        return '#6a79c2';
      case 'INTERCITY':
        return '#c2956a';
      default:
        return '#44b594';
    }
  };

  scrollY = new Animated.Value(0);
  HEADER_MAX_HEIGHT = 100;
  text = 666;
  HEADER_MIN_HEIGHT = 60;
  headerHeight = this.scrollY.interpolate({
    inputRange: [0, this.HEADER_MAX_HEIGHT - this.HEADER_MIN_HEIGHT],
    outputRange: [this.HEADER_MAX_HEIGHT, this.HEADER_MIN_HEIGHT],
    extrapolate: 'clamp',
  });
  textAnimation = this.scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [34, 26],
    extrapolate: 'clamp',
    easing: Easing.ease,
  });
  shadowAnimation = this.scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [0, 2],
    extrapolate: 'clamp',
  });
  shadowIosAnimation = this.scrollY.interpolate({
    inputRange: [0, 100],
    outputRange: [0, 0.2],
    extrapolate: 'clamp',
  });
  fadeAnimation = this.scrollY.interpolate({
    inputRange: [0, 50],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });
}

const stationsStore = new StationsStore();
export default stationsStore;
