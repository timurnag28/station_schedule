import {
  ActivityIndicator,
  Animated,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect} from 'react';
import store from '../store';
import {SfProTextRegular} from '../share/consts/consts';
import {observer} from 'mobx-react';
// @ts-ignore
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import StationsModal from './modals/StationsModal';
import TrainList from './components/TrainList';

const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);

const MainScreen = observer(() => {
  useEffect(() => {
    console.log('555');
    void store.stationsStore.onGetStationArrivals();
  }, []);

  return (
    <View style={styles.mainContainer}>
      <StationsModal />
      <AnimatedTouchable
        onPress={store.stationsStore.onShowStationsModal}
        style={styles.header}>
        <View style={{flex: 5}}>
          <Animated.Text numberOfLines={2} style={styles.headerTopText}>
            {store.stationsStore.currentStation}
          </Animated.Text>
          <Animated.Text style={styles.headerBottomText}>
            {`Last updated ${store.stationsStore.lastUpdateMinutes} minute ago`}
          </Animated.Text>
        </View>
        <Animated.View style={styles.iconStyle}>
          <IconFontAwesome
            style={{paddingRight: 18}}
            name={'chevron-right'}
            color={'#ffffff'}
            size={18}
          />
        </Animated.View>
      </AnimatedTouchable>
      {store.stationsStore.isLoading ? <Loader /> : <TrainList />}
    </View>
  );
});

const Loader = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <ActivityIndicator size="large" color="#fff" />
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#3ba270',
  },
  headerText: {
    color: '#fff',
    fontSize: 34,
    textAlign: 'left',
    fontFamily: SfProTextRegular,
    fontWeight: 'bold',
  },
  header: {
    flexDirection: 'row',
    height: store.stationsStore.headerHeight,
    backgroundColor: '#3ba270',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1000,
    paddingLeft: 26,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: store.stationsStore.shadowAnimation,
    },
    shadowOpacity: store.stationsStore.shadowIosAnimation,
    shadowRadius: 1.41,
    elevation: store.stationsStore.shadowAnimation,
  },
  headerTopText: {
    color: '#fff',
    fontSize: store.stationsStore.textAnimation,
    textAlign: 'left',
    fontFamily: SfProTextRegular,
    fontWeight: 'bold',
  },
  headerBottomText: {
    color: '#97cdb3',
    fontSize: 17,
    textAlign: 'left',
    fontFamily: SfProTextRegular,
    opacity: store.stationsStore.fadeAnimation,
  },
  iconStyle: {
    marginTop: 20,
    flex: 1,
    alignItems: 'flex-end',
    opacity: store.stationsStore.fadeAnimation,
  },
});

export default MainScreen;
