import React from 'react';
import {
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';

import {observer} from 'mobx-react';
import store from '../../store';
import {
  SfProTextRegular,
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../../share/consts/consts';
import {StationData} from '../../share/interfaces/interfaces';

const StationsModal = observer(() => {
  const {onShowStationsModal, isActiveStationModal} = store.stationsStore;
  return (
    <View style={{backgroundColor: '#fff'}}>
      <Modal
        animationInTiming={400}
        animationOutTiming={400}
        onBackButtonPress={onShowStationsModal}
        hideModalContentWhileAnimating={true}
        backdropOpacity={0.5}
        onBackdropPress={onShowStationsModal}
        onSwipeComplete={onShowStationsModal}
        swipeDirection={'down'}
        propagateSwipe={true}
        style={styles.modal}
        isVisible={isActiveStationModal}>
        <View style={styles.mainContainer}>
          <View style={styles.stick} />
          <Text style={styles.selectText}>Select Station</Text>
          <FlatList
            style={{marginTop: 32}}
            showsVerticalScrollIndicator={false}
            data={store.stationsStore.stationList}
            keyExtractor={(item: StationData) => item.StationCode}
            renderItem={({item}: {item: StationData}) => renderListItem(item)}
          />
          <View style={styles.footerContainer}>
            <TouchableOpacity
              onPress={store.stationsStore.onShowStationsModal}
              style={styles.footer}>
              <Text style={styles.closeText}>Close</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
});

const renderListItem = (item: StationData) => {
  return (
    <TouchableOpacity
      onPress={() => _onPressItem(item.StationCode, item.StationDesc)}>
      <View
        style={[
          styles.itemContainer,
          {
            backgroundColor:
              store.stationsStore.stationCode === item.StationCode
                ? '#3AA873'
                : '#fff',
          },
        ]}>
        <Text
          style={[
            styles.itemText,
            {
              color:
                store.stationsStore.stationCode === item.StationCode
                  ? '#fff'
                  : '#000',
            },
          ]}>
          {item.StationDesc}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const _onPressItem = (StationCode: string, StationDesc: string) => {
  void store.stationsStore.onSelectStation(StationCode, StationDesc);
};

const styles = StyleSheet.create({
  modal: {
    marginHorizontal: 0,
    marginTop: Platform.OS === 'ios' ? 70 : 20,
    marginBottom: 'auto',
    maxHeight: WINDOW_HEIGHT,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopStartRadius: 40,
    borderTopEndRadius: 40,
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  stick: {
    width: 46,
    height: 5,
    backgroundColor: '#000',
    marginTop: 10,
    opacity: 0.2,
    borderRadius: 12,
  },
  selectText: {
    fontSize: 23,
    color: '#000',
    fontWeight: 'bold',
    fontFamily: SfProTextRegular,
    marginTop: 24,
  },
  footerContainer: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderWidth: 1,
    borderColor: '#e8e5e5',
    width: WINDOW_WIDTH,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingVertical: 12,
    borderWidth: 2,
    borderColor: '#E5E5E5',
    borderRadius: 42,
    marginVertical: 20,
    width: WINDOW_WIDTH / 2.5,
    backgroundColor: '#fff',
  },
  closeText: {
    fontSize: 19,
    color: '#000',
    fontFamily: SfProTextRegular,
    fontWeight: 'bold',
  },
  itemContainer: {
    flex: 1,
    width: WINDOW_WIDTH * 0.95,
    borderRadius: 16,
    paddingLeft: 20,
    paddingVertical: 19.5,
  },
  itemText: {
    fontSize: 19,
    fontFamily: SfProTextRegular,
    fontWeight: 'bold',
  },
});

export default StationsModal;
