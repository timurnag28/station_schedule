import React from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {observer} from 'mobx-react';
import store from '../../store';
import {SfProTextRegular, WINDOW_HEIGHT} from '../../share/consts/consts';

const BottomTab = observer(() => {
  return (
    <View style={styles.bottomTab}>
      <View style={styles.mainContainer}>
        <TouchableOpacity
          style={{flex: 1}}
          onPress={() => Alert.alert('Settings')}>
          <Text style={styles.settings}>Settings</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={store.stationsStore.onSetDirection}
          style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('./../../../assets/iconsImages/compass.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{flex: 1, alignItems: 'flex-end'}}
          onPress={() => Alert.alert('Info')}>
          <Text style={styles.info}>Info</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  bottomTab: {
    backgroundColor: '#3AA873',
    height: WINDOW_HEIGHT / 14,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  mainContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 35,
    marginTop: 16,
    flex: 1,
  },
  settings: {
    fontSize: 17,
    color: '#fff',
    fontFamily: SfProTextRegular,
    fontWeight: 'bold',
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  image: {
    width: WINDOW_HEIGHT / 8,
    height: WINDOW_HEIGHT / 8,
    marginBottom: 50,
    resizeMode: 'contain',
  },
  info: {
    fontSize: 17,
    color: '#fff',
    fontFamily: SfProTextRegular,
    fontWeight: 'bold',
  },
});

export default BottomTab;
