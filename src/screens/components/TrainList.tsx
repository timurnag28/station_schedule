import React from 'react';
import {
  Alert,
  Animated,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {observer} from 'mobx-react';
import store from '../../store';
import {
  SfProTextBold,
  SfProTextRegular,
  WINDOW_HEIGHT,
} from '../../share/consts/consts';
import {TrainData} from '../../share/interfaces/interfaces';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

const TrainList = observer(() => {
  return (
    <View style={styles.mainContainer}>
      <FlatList
        showsVerticalScrollIndicator={false}
        style={{
          marginTop: store.stationsStore.headerLettersNumber > 16 ? 40 : 4,
        }}
        contentContainerStyle={{
          paddingTop: 90,
          paddingBottom: WINDOW_HEIGHT / 13.7,
        }}
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {y: store.stationsStore.scrollY},
              },
            },
          ],
          {
            useNativeDriver: false,
          },
        )}
        data={store.stationsStore.sortedTrainsList}
        renderItem={({item, index}) => renderListItem(item, index)}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={<EmptyListText />}
      />
    </View>
  );
});

const renderListItem = (item: TrainData, index: number) => {
  return (
    <View style={styles.item}>
      <View style={styles.innerContainer}>
        <View style={styles.firstPart}>
          <View
            style={[
              styles.topTextContainer,
              {
                backgroundColor: store.stationsStore.determineViewColor(
                  item.Traintype,
                ),
              },
            ]}>
            <Text
              style={[
                styles.topText,
                {
                  color: store.stationsStore.determineTextColor(item.Traintype),
                },
              ]}>
              {item.Traintype}
            </Text>
          </View>
          <View style={{flex: 1}}>
            <Text style={styles.middleText}>{item.Destination}</Text>
          </View>
          <TouchableOpacity
            onPress={() => Alert.alert('Set alarm')}
            style={{flex: 1}}>
            {index === 2 ? (
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <IconFontAwesome
                  style={{paddingRight: 6}}
                  name={'bell'}
                  color={'#f07356'}
                  size={18}
                />
                <Text style={styles.iconText}>15 min</Text>
              </View>
            ) : (
              <Text style={styles.bottomText}>Set Alarm</Text>
            )}
          </TouchableOpacity>
        </View>
        <View style={styles.secondPart}>
          <View>
            <Text style={styles.number}>{item.Duein}</Text>
          </View>
          <Text style={styles.minutes}>min</Text>
        </View>
      </View>
    </View>
  );
};

const EmptyListText = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: WINDOW_HEIGHT / 3.5,
      }}>
      <Text
        style={{
          fontSize: 14,
          fontFamily: SfProTextRegular,
          fontWeight: 'bold',
          color: '#fff',
        }}>
        No trains available.
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 8,
    marginHorizontal: 16,
    paddingVertical: 12,
    paddingLeft: 24,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: '#3ba270',
  },
  innerContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
  },
  firstPart: {
    flex: 4,
    justifyContent: 'space-around',
  },
  secondPart: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftWidth: 1,
    borderColor: '#E5E5E5',
    height: '90%',
  },
  topTextContainer: {
    flex: 1,
    alignSelf: 'flex-start',
    borderRadius: 5,
  },
  topText: {
    fontFamily: SfProTextRegular,
    fontSize: 14,
    paddingHorizontal: 8.5,
    paddingVertical: 4.5,
    fontWeight: 'bold',
    letterSpacing: 1,
  },
  middleText: {
    fontSize: 26,
    fontFamily: SfProTextRegular,
    fontWeight: 'bold',
    color: '#000',
  },
  bottomText: {
    fontFamily: SfProTextRegular,
    fontSize: 14,
    color: '#a0a0a0',
    marginTop: 5,
  },
  iconText: {
    fontFamily: SfProTextBold,
    fontSize: 14,
    color: '#f07356',
    marginTop: 5,
    fontWeight: 'bold',
  },
  number: {
    fontFamily: SfProTextRegular,
    fontSize: 34,
    color: '#000',
    fontWeight: 'bold',
  },
  minutes: {
    fontFamily: SfProTextRegular,
    fontSize: 14,
    color: '#acacac',
    fontWeight: 'bold',
    letterSpacing: 2,
  },
});

export default TrainList;
