export interface StationData {
  StationDesc: string;
  StationLatitude: number;
  StationLongitude: number;
  StationCode: string;
  StationType: string[];
  StationId: number;
}

export interface TrainData {
  Servertime: string;
  Traincode: string;
  Stationfullname: string;
  Stationcode: string;
  Querytime: string;
  Traindate: string;
  Origin: string;
  Destination: string;
  Origintime: string;
  Destinationtime: string;
  Status: string;
  Lastlocation: string | {};
  Duein: number;
  Late: number;
  Exparrival: string;
  Expdepart: string;
  Scharrival: string;
  Schdepart: string;
  Direction: string;
  Traintype: string;
  Locationtype: string;
}
