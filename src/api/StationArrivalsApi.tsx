export function onGetStationArrivals(StationCode: string): Promise<any> {
  const url = `https://api.darto.ie/station/${StationCode}`;
  const options = {method: 'GET', headers: {accept: 'application/json'}};
  return fetch(url, options)
    .then(response => response.json())
    .then(responseJson => {
      return responseJson;
    })
    .catch(err => {
      console.error(err);
      throw err;
    });
}
