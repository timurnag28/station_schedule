import React from 'react';
import {SafeAreaView, StatusBar, View} from 'react-native';

import FlashMessage from 'react-native-flash-message';
import BottomTab from './src/screens/components/BottomTab';
import MainScreen from './src/screens/MainScreen';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#3ba16f'}}>
      <StatusBar translucent backgroundColor="'rgba(0, 0, 0, 0.1)'" />
      <FlashMessage
        hideStatusBar={false}
        statusBarHeight={StatusBar.currentHeight}
        position="top"
      />
      <View
        style={{
          flex: 1,
          backgroundColor: '#33b476',
          paddingTop: StatusBar.currentHeight,
        }}>
        <MainScreen />
      </View>
      <BottomTab />
    </SafeAreaView>
  );
};

export default App;
